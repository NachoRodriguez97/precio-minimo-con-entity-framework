﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrecioMinimo.Models;

namespace PrecioMinimo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (tiendaEntities p = new tiendaEntities())
            {
                var prod = p.producto.SqlQuery("MostrarProductos").ToList();
                dataGridView1.DataSource = prod.ToList();
            }
        }

        private void btnObtenerDatos_Click(object sender, EventArgs e)
        {
            using (tiendaEntities p = new tiendaEntities())
            {
                var prod = p.producto.SqlQuery("PrecioMinimo @precioMinimo", new SqlParameter("@precioMinimo", Convert.ToDouble(textBox1.Text)));
                dataGridView1.DataSource = prod.ToList();
            }
        }
        private void btnGuardarDatos_Click(object sender, EventArgs e)
        {
            using (tiendaEntities p = new tiendaEntities())
            {
                try
                {
                    Resultado_Winform r = new Resultado_Winform();
                    r.Fecha = DateTime.Now;
                    r.CantidadDeRegistros = p.producto.SqlQuery("PrecioMinimo @precioMinimo", new SqlParameter("@precioMinimo", Convert.ToDouble(textBox1.Text))).Count();
                    p.Resultado_Winform.Add(r);
                    p.SaveChanges();
                    MessageBox.Show("Los registros fueron insertados correctamente");
                    textBox1.Clear();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }
            }
        }

        private void btnMostrarDatos_Click(object sender, EventArgs e)
        {
            using (tiendaEntities p = new tiendaEntities())
            {
                var prod = p.Resultado_Winform.SqlQuery("MostrarResultado_Winform").ToList();
                dataGridView1.DataSource = prod.ToList();
            }
        }
    }

}

     